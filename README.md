## Video Preview

[![video](https://i9.ytimg.com/vi/ibxEiRFToRA/mq3.jpg?sqp=CISNzJ8G&rs=AOn4CLB_rvq0jieI0jDvFJVkBp93Afq8SA&retry=3)](https://www.youtube.com/watch?v=ibxEiRFToRA)

## Main Files

**\Source\TestAssignmentTA\NiagaraSplineSystem.h(cpp)** - system source files \
**\Content\General\Actors\BP_NiagaraSplineSystem** - system bp asset \
**\Content\General\Materials\M_NiagaraSplineSystem** - system material (with fade out timer and color change) \
**\Content\General\Niagara\NS_SplineRibbon** - niagara system with ribbon emmiter

## 1

**The system takes an array of vectors and creates a spline for the Niagara Component**

- Niagara system gets 2 vector arrays as user parameters: projected location, projected normal;
- Locations (before projection) are evenly distributed points on the helper spline component;
- Distribution controled by Construction.SplineSubdivisionStep actor variable;
- Helper spline component created by using 3D-Widget vector array Contruction.SplinePoints;
- As for me, it's much more easier to build a spline using those 3D-Widgets then with built-in spline control system.

## 2

**The system should fade out and appear by an event. The timer should be created in the ribbon material**

- System instantly fade out by using StartFadeOut() function;
- Can be called in editor in Material section;
- Timer length determed in Materil.FadeOutTimer actor variable;
- Time information passed to niagara system parameter binded with material parameter.

## 3

**The system should smoothly change color by an event**

- System could change color smoothly by using StartColorChange() function;
- Can be called in editor in Material section;
- It's a single HueShift loop by given time amount (Material.ColorChangeTime);
- Time information passed to niagara system parameters binded with material parameters;
- System color base color can be changed by Materil.BaseColor actor variable.

## 4

**The system should take the normal of the surface and change the ribbon orientation regarding to this normal (projecting on the surface).**

- System pass surface normals to niagara system;
- Surface normals obtained after raycast from distributed spline locations to FVector::DownVector direction;
- Actor rotation affects porjection;
- Ribbon projected location can be sweeped to surface normal direction (Projection.ProjectionSweep actor parameter);
- Also projection goes well with vertical surfaces. 

## 5

**The system doesn’t have a life time**

- Niagara system have infinite loop behavior;
- System actor don't have life span;
- Although there are might be a small pop-up effect after any parameter change (I guess it's because of system rebuild in construction script).

## Other
#### System stability
System might not behave well with a big (ribbon amount/spline length) ratio, especially with a lot of vertical surfaces. It can be fixed by increasing Projection.Vertical.VerticalProjectionStep or Construction.SplineSubdivisionStep values.
#### Projection on level load
Projection might ignore other actors after level load. Currently it's fixed by a small timer by a small timer for first rebuild. I guess it's not a best practice, but I didn't found a right event to build the system first time after all other actor fully loaded (also I don't want it to rebuild every tick).
#### Closed loop
I didn't find a way to create closed loop properly. So, there are a few options for different situations (mostly it's angle-dependent): 
| Overlap with subdivison | 
| ------ | 
| ![](https://gitlab.com/Qtuerr/test-assignment/-/raw/main/ReadmeSupply/CycleOverlapWithSubdivision.jpg) |
| Subdivide spline around start and connect this small segments |

| Overlap  | 
| ------ | 
| ![](https://gitlab.com/Qtuerr/test-assignment/-/raw/main/ReadmeSupply/Overlap.jpg) |
| Overlap first original segment to close a gap |

| Gap  | 
| ------ | 
| ![](https://gitlab.com/Qtuerr/test-assignment/-/raw/main/ReadmeSupply/Gap.jpg) |
| No connection, but also no translucent overlap|

