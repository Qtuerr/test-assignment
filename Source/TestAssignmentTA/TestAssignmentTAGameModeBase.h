// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestAssignmentTAGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTASSIGNMENTTA_API ATestAssignmentTAGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
