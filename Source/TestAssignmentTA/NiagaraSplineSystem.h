#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Components/SplineComponent.h"
#include "NiagaraSplineSystem.generated.h"

// Magic numbers for system stability with low values
#define VERTICAL_PROJECTION_MIN_RATE 0.01f
#define PROJECTION_MIN_RATE 0.01f

struct TESTASSIGNMENTTA_API FProjectedLocation {
	FVector WorldSweepedHitLocation;
	FVector LocalHitLocation;
	FVector LocalHitNormal;
};

UCLASS()
class TESTASSIGNMENTTA_API ANiagaraSplineSystem : public AActor
{
	GENERATED_BODY()
	
public:	
	ANiagaraSplineSystem();

protected:
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform &Transform) override;

	virtual void Tick(float DeltaTime) override;

public:
	USceneComponent* DummyRootComponent;
	USplineComponent* SplineHelperComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bRebuildOnConstruction;

	// Please leave false for sanity
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bRebuildOnTick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Niagara")
	UNiagaraComponent* NiagaraSystemComponent;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName LocationArrayParamName;
	
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName NormalArrayParamName;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName BaseColorParamName;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName TimerFadeOutParamName;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName TimerColorChangeParamName;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName TimeAmountColorChangeParamName;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category="Niagara")
	FName WidthParamName;

	UPROPERTY(EditAnywhere, Category="Material")
	FLinearColor BaseColor;

	UPROPERTY(EditAnywhere, Category="Material")
	float FadeOutTimer;

	UPROPERTY(EditAnywhere, Category="Material")
	float ColorChangeTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (MakeEditWidget = true), Category="Construction")
	TArray<FVector> SplinePoints;

	UPROPERTY(EditAnywhere, Category="Construction")
	float RibbonWidth;

	UPROPERTY(EditAnywhere, Category="Construction")
	int32 SplineSubdivisionStep;

	UPROPERTY(EditAnywhere, Category="Construction")
	bool bClosedLoop;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bClosedLoop", EditConditionHides), Category="Construction")
	bool bSmallSubdivisionOnStart;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bClosedLoop", EditConditionHides), Category="Construction")
	bool bClosedLoopWithOverlap;

	UPROPERTY(EditAnywhere, Category="Projection")
	bool bProjection;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection", EditConditionHides), Category="Projection")
	TEnumAsByte<ECollisionChannel> ProjectionTraceChannel;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection", EditConditionHides), Category="Projection")
	float ProjectionSweep;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection", EditConditionHides), Category="Projection")
	bool bVerticalProjection;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection && bVerticalProjection", EditConditionHides), Category="Projection|Vertical")
	float VerticalProjectionTrashold;

	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection && bVerticalProjection", EditConditionHides), Category="Projection|Vertical")
	float VerticalProjectionStep;
	
	UPROPERTY(EditAnywhere, meta=(EditCondition="bProjection && bVerticalProjection", EditConditionHides), Category="Projection|Vertical")
	float VerticalProjectionSweep;

// OnLoad delay
private:
	bool bFirstTimer = true;
	FTimerHandle FirstTimeBuilderHandle;
	
	void FirstTimeLoadRebuild(); 

// Spline building
private:
	UFUNCTION(BlueprintCallable, CallInEditor, Category="Construction")
	void Rebuild();

	void BuildAndProjectSpline(TArray<FVector>& Locations, TArray<FVector>& Normals);

	void RebuildHelperSpline();
	TArray<FVector> SubdivideSpline();
	void SubdivideStartLocation(TArray<FVector>& Locations, const FVector& TangentOnStart);

	void TryProjectOnSurface(const TArray<FVector>& TraceLocations, 
							TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals);
	void ReturnUnprojected(const TArray<FVector>& TraceLocations, 
							TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals);
	void ProjectOnSurface(const TArray<FVector>& TraceLocations, 
							TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals);

	bool ProjectSingleLocation(FVector WorldStartTraceLocation, FVector WorldEndTraceLocation, 
							FProjectedLocation& ProjectedLocation, float SweepAmount);
	
	void TryVerticalProjection(const FVector& PrevProjectedLocation, const FVector& CurrentProjectedLocation, 
								TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals);
	void DoVerticalProjection(const FVector& PrevProjectedLocation, const FVector& CurrentProjectedLocation, 
								TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals);
	

	void CloseSplineLoop(TArray<FVector>& Locations, TArray<FVector>& Normals);
	void CloseSplineLoopWithGap(TArray<FVector>& Locations, TArray<FVector>& Normals);
	void CloseSplineLoopWithOverlap(TArray<FVector>& Locations, TArray<FVector>& Normals);

// Niagara params pass
private:
	void PassBuildParamsToNiagaraSystem(TArray<FVector>& Locations, TArray<FVector>& Normals);

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Material")
	void StartFadeOut();

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Material")
	void StartColorChange();

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Material")
	void ResetMaterialTimers();

	void PassWidttParamToNiagaraSystem(float Width);
	void PassVectorParamsToNiagaraSystem(const TArray<FVector>& Locations, const TArray<FVector>& Normals);
};
