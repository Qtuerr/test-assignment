// Fill out your copyright notice in the Description page of Project Settings.

#include "NiagaraDataInterfaceArrayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraSplineSystem.h"

ANiagaraSplineSystem::ANiagaraSplineSystem() {
	PrimaryActorTick.bCanEverTick = true;

	DummyRootComponent = CreateDefaultSubobject<USceneComponent>("DummyRootComponent");
	NiagaraSystemComponent = CreateDefaultSubobject<UNiagaraComponent>("NiagaraComponent");
	SplineHelperComponent = CreateDefaultSubobject<USplineComponent>("SplineHelper");

	NiagaraSystemComponent->SetupAttachment(DummyRootComponent);
	SplineHelperComponent->SetupAttachment(DummyRootComponent);

	SplineHelperComponent->bDrawDebug = false;

	bRebuildOnConstruction = true;
	bRebuildOnTick = false;

	BaseColor = FLinearColor(0.2f, 0.9f, 0.f, 1.f);
	FadeOutTimer = 5.f;
	ColorChangeTimer = 5.f;

	LocationArrayParamName = FName(TEXT("PoistionArray"));
	NormalArrayParamName = FName(TEXT("NormalArray"));
	BaseColorParamName = FName(TEXT("BaseColor"));
	TimerFadeOutParamName = FName(TEXT("FadeOutTimer"));
	TimerColorChangeParamName = FName(TEXT("ColorChangeTimer"));
	TimeAmountColorChangeParamName = FName(TEXT("ColorChangeTimeAmount")); 
	WidthParamName = FName(TEXT("RibbonWidth"));
	
	RibbonWidth = 30.f;
	SplineSubdivisionStep = 55.f;
	ProjectionTraceChannel = ECollisionChannel::ECC_Visibility;
	bProjection = true;
	bVerticalProjection = true;
	VerticalProjectionTrashold = 100.f;
	VerticalProjectionStep = 10.f;
	ProjectionSweep = VerticalProjectionSweep = 1.f;
	bClosedLoop = true;
	bSmallSubdivisionOnStart = true;
	bClosedLoopWithOverlap = true;
	
}


// First time construction script  should run after others actors initialization
// Otherwise there will be troubles with projection
void ANiagaraSplineSystem::FirstTimeLoadRebuild() {
	Rebuild();
}

void ANiagaraSplineSystem::OnConstruction(const FTransform &Transform) {
	if (bFirstTimer) {
		GetWorld()->GetTimerManager().SetTimer(FirstTimeBuilderHandle, this, &ANiagaraSplineSystem::FirstTimeLoadRebuild, 1.f, false);
		bFirstTimer = false;
		return;
	}
	if (bRebuildOnConstruction) {
		Rebuild();
	}
}

void ANiagaraSplineSystem::BeginPlay() {
	Super::BeginPlay();
	Rebuild();
}

void ANiagaraSplineSystem::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if (bRebuildOnTick) {
		Rebuild();
	}
}

void ANiagaraSplineSystem::TryProjectOnSurface(const TArray<FVector>& TraceLocations, TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals) {
	if (!bProjection) {
		ReturnUnprojected(TraceLocations, ProjectedLocations, ProjectedNormals);
		return;
	}
	ProjectOnSurface(TraceLocations, ProjectedLocations, ProjectedNormals);
}

void ANiagaraSplineSystem::ProjectOnSurface(const TArray<FVector>& TraceLocations, TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals) {
	FVector WorldPrevProjectedLocation;
	FProjectedLocation PrevProjectedLocation;
	FVector ProjectDirection = FVector::DownVector * 9000.f;
	for (auto& LocalTraceLocation: TraceLocations) {
		FVector WorldTraceLocation = GetTransform().TransformPosition(LocalTraceLocation);
		FProjectedLocation CurrentProjectedLocation;

		bool bIsHit = ProjectSingleLocation(WorldTraceLocation, WorldTraceLocation + ProjectDirection, CurrentProjectedLocation, ProjectionSweep);
		if (bIsHit) {
			TryVerticalProjection(PrevProjectedLocation.WorldSweepedHitLocation, CurrentProjectedLocation.WorldSweepedHitLocation, 
								ProjectedLocations, ProjectedNormals);

			ProjectedLocations.Add(CurrentProjectedLocation.LocalHitLocation);
			ProjectedNormals.Add(CurrentProjectedLocation.LocalHitNormal);
			PrevProjectedLocation = CurrentProjectedLocation;
		}
	}
}

void ANiagaraSplineSystem::ReturnUnprojected(const TArray<FVector>& TraceLocations, TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals) {
	ProjectedLocations = TraceLocations;
	ProjectedNormals.Init(FVector::UpVector, ProjectedLocations.Num());
}

void ANiagaraSplineSystem::TryVerticalProjection(const FVector& PrevProjectedLocation, const FVector& CurrentProjectedLocation, 
												TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals) {
	bool bVerticalProjectionAvaliable = bVerticalProjectionAvaliable = ProjectedLocations.Num() > 0 && bVerticalProjection;
	bool bThasholdPassed = FMath::Abs(PrevProjectedLocation.Z - CurrentProjectedLocation.Z) > VerticalProjectionTrashold;
	if (bVerticalProjectionAvaliable && bThasholdPassed) {
		DoVerticalProjection(PrevProjectedLocation, CurrentProjectedLocation, ProjectedLocations, ProjectedNormals);
	}
}

void ANiagaraSplineSystem::DoVerticalProjection(const FVector& PrevProjectedLocation, const FVector& CurrentProjectedLocation, 
								TArray<FVector>& ProjectedLocations, TArray<FVector>& ProjectedNormals) {
	float VerticalLength = FMath::Abs(PrevProjectedLocation.Z - CurrentProjectedLocation.Z);
	float VerticalStep = FMath::Max(VerticalProjectionStep, VERTICAL_PROJECTION_MIN_RATE * VerticalLength);
	bool bPrevLocationLower = PrevProjectedLocation.Z < CurrentProjectedLocation.Z;
	float StepDirectionMultiplier = bPrevLocationLower? 1: -1;
	FVector CastFrom = bPrevLocationLower? PrevProjectedLocation: FVector(CurrentProjectedLocation.X, CurrentProjectedLocation.Y, PrevProjectedLocation.Z);
	FVector CastTo = bPrevLocationLower? CurrentProjectedLocation: FVector(PrevProjectedLocation.X, PrevProjectedLocation.Y, CurrentProjectedLocation.Z);
	FVector CastDirection = (CastTo - CastFrom) * FVector(1.f, 1.f, 0.f);
	for (float ItVerticalStep = VerticalStep; ItVerticalStep < VerticalLength; ItVerticalStep += VerticalStep) {
		FVector ItCastFrom = CastFrom + FVector::UpVector * ItVerticalStep * StepDirectionMultiplier;
		FVector ItCastTo = ItCastFrom + CastDirection;
		FProjectedLocation ProjectedVerticalLocation;
		//DrawDebugLine(GetWorld(),ItCastFrom, ItCastTo,FColor::Green,0,100000.f,0,4.f);
		bool bIsHit = ProjectSingleLocation(ItCastFrom, ItCastTo, ProjectedVerticalLocation, VerticalProjectionSweep);
		if (bIsHit) {
			//DrawDebugPoint(GetWorld(),ProjectedVerticalLocation.WorldSweepedHitLocation,10.f,FColor::Red,0,100000.f,0);
			ProjectedLocations.Add(ProjectedVerticalLocation.LocalHitLocation);
			ProjectedNormals.Add(ProjectedVerticalLocation.LocalHitNormal);
		}
	}
}

bool ANiagaraSplineSystem::ProjectSingleLocation(FVector WorldStartTraceLocation, FVector WorldEndTraceLocation, FProjectedLocation& ProjectedLocation, float SweepAmount) {
	FHitResult HitResult;
	if (GetWorld()->LineTraceSingleByChannel(HitResult, WorldStartTraceLocation, WorldEndTraceLocation, ProjectionTraceChannel)) {
		ProjectedLocation.WorldSweepedHitLocation = HitResult.Location + HitResult.Normal * SweepAmount;
		ProjectedLocation.LocalHitLocation = GetTransform().InverseTransformPosition(ProjectedLocation.WorldSweepedHitLocation);
		ProjectedLocation.LocalHitNormal = GetTransform().InverseTransformPosition(GetActorLocation() - HitResult.Normal);
		return true;
	}
	return false;
}

TArray<FVector> ANiagaraSplineSystem::SubdivideSpline() {
	check(SplineHelperComponent);

	TArray<FVector> ResultLocations;
	float SplineLength = SplineHelperComponent->GetSplineLength();
	float OriginalPointsCount = SplinePoints.Num();

	float SubdivisionStep = FMath::Max(SplineSubdivisionStep, PROJECTION_MIN_RATE * SplineLength);
	for (float ItStep = 0; ItStep < SplineLength; ItStep += SubdivisionStep) {
		FVector CurrentStepLocation = SplineHelperComponent->GetLocationAtDistanceAlongSpline(ItStep, ESplineCoordinateSpace::Type::Local);
		ResultLocations.Add(CurrentStepLocation);
	}
	
	
	FVector TangentOnStart = SplineHelperComponent->GetTangentAtSplinePoint(0, ESplineCoordinateSpace::Type::Local).GetSafeNormal();
	SubdivideStartLocation(ResultLocations, TangentOnStart);

	return ResultLocations;
}

void ANiagaraSplineSystem::SubdivideStartLocation(TArray<FVector>& Locations, const FVector& TangentOnStart) {
	if (!bClosedLoop) return;
	if (!bSmallSubdivisionOnStart) return;
	if (Locations.IsEmpty()) return;

	TArray<FVector> LocationsTMP;
	FVector StartLocaiton = Locations[0];
	LocationsTMP.Add(StartLocaiton);
	LocationsTMP.Add(StartLocaiton + TangentOnStart * 0.6f);
	Locations.RemoveAt(0);
	LocationsTMP.Append(Locations);
	LocationsTMP.Add(StartLocaiton - TangentOnStart * 0.6f);
	Locations = LocationsTMP;
}

void ANiagaraSplineSystem::Rebuild() {
	TArray<FVector> Locations;
	TArray<FVector> Normals;

	BuildAndProjectSpline(Locations, Normals);
	PassBuildParamsToNiagaraSystem(Locations, Normals);
}

void ANiagaraSplineSystem::BuildAndProjectSpline(TArray<FVector>& Locations, TArray<FVector>& Normals) {
	bool bIsAnyPointExists = SplinePoints.Num() > 0;
	if (bIsAnyPointExists) {
		RebuildHelperSpline();
		TArray<FVector> UnprojectedLocations = SubdivideSpline();
		TryProjectOnSurface(UnprojectedLocations, Locations, Normals);
		if (bClosedLoop) {
			CloseSplineLoop(Locations, Normals);
		}
	}
}



void ANiagaraSplineSystem::RebuildHelperSpline() {
	if (SplineHelperComponent) {
		SplineHelperComponent->SetClosedLoop(bClosedLoop, false);
		SplineHelperComponent->ClearSplinePoints(false);
		SplineHelperComponent->SetSplinePoints(SplinePoints, ESplineCoordinateSpace::Type::Local);
	}
}

void ANiagaraSplineSystem::CloseSplineLoop(TArray<FVector>& Locations, TArray<FVector>& Normals) {
	if (!bClosedLoop) return;

	if (bClosedLoopWithOverlap) {
		CloseSplineLoopWithOverlap(Locations, Normals);
	} 
	else {
		CloseSplineLoopWithGap(Locations, Normals);
	}
}

void ANiagaraSplineSystem::CloseSplineLoopWithGap(TArray<FVector>& Locations, TArray<FVector>& Normals) {
	if (Locations.IsValidIndex(0) && Normals.IsValidIndex(0)) {
		FVector FirstLocation = Locations[0];
		FVector FirstNormal = Normals[0];
		Locations.Add(FirstLocation);
		Normals.Add(FirstNormal);
	}
}

void ANiagaraSplineSystem::CloseSplineLoopWithOverlap(TArray<FVector>& Locations, TArray<FVector>& Normals) {
	if (Locations.IsValidIndex(1) && Normals.IsValidIndex(1)) {
		FVector SecondLocation = Locations[1];
		FVector SecondNormal = Normals[1];
		Locations.Add(SecondLocation);
		Normals.Add(SecondNormal);
	}
}

void ANiagaraSplineSystem::PassBuildParamsToNiagaraSystem(TArray<FVector>& Locations, TArray<FVector>& Normals) {
	PassVectorParamsToNiagaraSystem(Locations, Normals);
	NiagaraSystemComponent->SetVariableLinearColor(BaseColorParamName, BaseColor);
	ResetMaterialTimers();
	PassWidttParamToNiagaraSystem(RibbonWidth);
}

void ANiagaraSplineSystem::StartFadeOut() {
	check(NiagaraSystemComponent);

	float CurrentTime = UGameplayStatics::GetTimeSeconds(GetWorld());

	NiagaraSystemComponent->SetVariableFloat(TimerFadeOutParamName, CurrentTime + FadeOutTimer);
}

void ANiagaraSplineSystem::StartColorChange() {
	check(NiagaraSystemComponent);

	float CurrentTime = UGameplayStatics::GetTimeSeconds(GetWorld());

	NiagaraSystemComponent->SetVariableFloat(TimerColorChangeParamName, CurrentTime + ColorChangeTimer);
	NiagaraSystemComponent->SetVariableFloat(TimeAmountColorChangeParamName, ColorChangeTimer);
}

void ANiagaraSplineSystem::ResetMaterialTimers() {
	check(NiagaraSystemComponent);
	
	NiagaraSystemComponent->SetVariableFloat(TimerFadeOutParamName, 0.f);
	NiagaraSystemComponent->SetVariableFloat(TimerColorChangeParamName, 0.f);
	NiagaraSystemComponent->SetVariableFloat(TimeAmountColorChangeParamName, 0.f);
}

void ANiagaraSplineSystem::PassWidttParamToNiagaraSystem(float Width) {
	check(NiagaraSystemComponent);

	NiagaraSystemComponent->SetVariableFloat(WidthParamName, Width);
}

void ANiagaraSplineSystem::PassVectorParamsToNiagaraSystem(const TArray<FVector>& Locations, const TArray<FVector>& Normals) {
	check(NiagaraSystemComponent);

	UNiagaraDataInterfaceArrayFunctionLibrary::SetNiagaraArrayVector(NiagaraSystemComponent, 
																	LocationArrayParamName, Locations);
	UNiagaraDataInterfaceArrayFunctionLibrary::SetNiagaraArrayVector(NiagaraSystemComponent, 
																	NormalArrayParamName, Normals);
}
